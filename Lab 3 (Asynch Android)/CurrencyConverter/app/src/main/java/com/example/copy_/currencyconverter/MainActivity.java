package com.example.copy_.currencyconverter;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    //Declare some variables
    private EditText editText01;
    private Button bnt01;
    private TextView textView01;
    private String usd;
    // real time access to conversion
    private static final String url = "https://api.fixer.io/latest?base=USD";
    // json object
    String json = "";
    String line = "";
    String rate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //cast the variables to their ids
        editText01 = findViewById(R.id.EditText01);
        bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);

        //Click event
        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View convertToYen){
                System.out.println("\nTESTING 1...Before Asynch");

                BackgroundTask object = new BackgroundTask();

                object.execute();

                System.out.println("\nTesting2 ... after Asynch");
            }
        });
    }

    private class BackgroundTask extends AsyncTask<String, Void, String> {
        // the method we use to specify what happens before asynch
        @Override
        protected void onPreExecute() { super.onPreExecute(); }
        @Override
        protected void onProgressUpdate(Void...values) { super.onProgressUpdate(values);}
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            System.out.println("\nWhat is rate: " + rate + "\n");
            //convert rate from string to double
            Double value = Double.parseDouble(rate);

            System.out.println("\nTesting JSON StringExchange Rate inside AsynchTask: " + value);

            //covert user's input to string
            usd = editText01.getText().toString();
            //if-else statement to make sure user cannot leave the EditText blank
            if (usd.equals("")) {
                textView01.setText("This field cannot be blank!");
            } else {
                //Convert string to double
                Double dInputs = Double.parseDouble(usd);
                //Convert function
                Double output = dInputs * value;
                //Display the result
                textView01.setText("$" + usd + " = " + "¥" + String.format("%.2f", output));
                //clear the edittext after clicking
                editText01.setText("");
            }
        }

        @Override
        protected String doInBackground(String...params) {

            try {
                // create url class object and initialize it
                URL web_url = new URL(MainActivity.this.url);

                // create httpURL connection class object and initialize it
                HttpURLConnection httpURLConnection = (HttpURLConnection) web_url.openConnection();

                //GET set as request method
                httpURLConnection.setRequestMethod("GET");

                System.out.println("\n TESTING ... before connection method to URL\n");

                // invokes the connect() method from the object httpURLConnection
                httpURLConnection.connect();

                // create an InputStream and initialize the object
                InputStream inputStream = httpURLConnection.getInputStream();

                // create a bufferedReader from BufferedReader class and initialize the object with new BufferedReader
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                System.out.println("\nCONNECTION SUCCESSFUL\n");

                // extracting string information from JSON line by line using while loop
                while (line != null) {
                    //assign the bufferedReader.readLine() to the string line every iteration
                    line = bufferedReader.readLine();
                    // append line to json object
                    json += line;
                }
                System.out.println("\nThe JSON: " + json);

                //create JSON object and initialize new
                JSONObject obj = new JSONObject(json);

                //creates a nested JSON object inside first JSONObject
                JSONObject objRate = obj.getJSONObject("rates");

                // we use "JPY" to obtain rat, the logic code for Currency Conversion must
                // go into onPostExecute() within AsyncTask
                rate = objRate.get("JPY").toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON expectation", e);
                System.exit(1);
            }
            return null;
        }
    }
}
